# docker build -t hack .
# docker run -it --rm -p 7227:7227 --hostname localhost hack
# Find the URL in the console

# Per jupyter docker-stacks:
# https://github.com/jupyter/docker-stacks/tree/master/base-notebook
# http://jupyter-docker-stacks.readthedocs.io/en/latest/index.html
FROM jupyter/base-notebook

# Install jupyterlab widget manager (needed for custom widgets)
# For some reason, must install this *before* local extension
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager

# Install our lab extension - must do this as root user
WORKDIR /home/$NB_USER/work/hack
ADD ./ ./
USER root
RUN jupyter labextension install .
USER $NB_USER
WORKDIR /

# Setup entry point
EXPOSE 7227
ENTRYPOINT ["jupyter", "lab", "--ip=0.0.0.0", "--port=7227", "--allow-root", "--no-browser"]
