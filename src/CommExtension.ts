/*
 * Comm widget extension
 * Loading this widget requires installing jupyter widgets extension:
 * jupyter labextension install @jupyter-widgets/jupyterlab-manager
 * per https://gitter.im/jupyter-widgets/Lobby?at=5ae2a2467c3a01610d2689c5
 */
import { JupyterLab, JupyterLabPlugin } from '@jupyterlab/application';
import { ISettingRegistry } from "@jupyterlab/coreutils";
import { DocumentRegistry } from '@jupyterlab/docregistry';
import { NotebookPanel, INotebookModel } from '@jupyterlab/notebook';

import { IJupyterWidgetRegistry } from '@jupyter-widgets/base';

import { IDisposable, DisposableDelegate } from '@phosphor/disposable';

import CommRegistry from './CommRegistry';

export
class CommWidgetExtension implements DocumentRegistry.IWidgetExtension<NotebookPanel, INotebookModel> {
  createNew(panel: NotebookPanel, context: DocumentRegistry.IContext<INotebookModel>): IDisposable {
    Promise.all([panel.revealed, panel.session.ready, context.ready]).then(() => {
      // Register message handler
      const session = context.session;
      const kernelInstance = session.kernel;
      kernelInstance.registerCommTarget('hack_comm_target', function(comm:any, msg:any) {
        CommRegistry.registerComm(comm, msg);
      });
    });

    return new DisposableDelegate(() => {});
   }
 };


export
const CommExtension: JupyterLabPlugin<void> = {
  id: 'jupyterlab_hack:comm',
  autoStart: true,
  requires: [IJupyterWidgetRegistry, ISettingRegistry],
  activate: (app: JupyterLab, ) => {
    console.log('commPlugin activated!');
    app.docRegistry.addWidgetExtension('Notebook', new CommWidgetExtension());
  },
};
