/*
 * An example JupyterLab extension package with 2 extensions:
 *   A comm extension for sending data back to the Jupyter kernel
 *   A simple form extension for user input
 */

import { JupyterLab, JupyterLabPlugin } from '@jupyterlab/application';
import { Contents, ContentsManager }   from '@jupyterlab/services';
import { CommExtension } from './CommExtension';
import { SimpleFormMimeExtension } from './SimpleFormMimeExtension'
import { createRendermimePlugin } from './mimerenderers';

import * as foobar from '../lib/JUPYTERLAB_FILE_LOADER_foobarlib.js';


// Quick test of contents manager
export
const ServicesExtension: JupyterLabPlugin<void> = {
  id: 'jupyterlab_hack:services',
  autoStart: true,
  requires: [],
  activate: (app: JupyterLab, ) => {
    console.log('services activated!');

    console.log(`foobar: ${foobar}`);
    console.dir(foobar);

    // console.log(`foobar.Foo: ${foobar.Foo}`);
    // console.log(`type: ${typeof foobar.Foo}`);
    // console.dir(foobar.Foo);
    let foo:any = new foobar.Foo();
    console.log('Foo instance:');
    console.dir(`${foo}`);
    console.log(foo.getMessage());

    let bar:any = new foobar.Bar();
    console.log(`${bar}`);

    const cm: ContentsManager = app.serviceManager.contents;
    console.log('content manager:');
    console.dir(cm);
    //let path: string = '/home/john/temp/track3p.sbt';  // 404
    let path: string = 'setup.py';
    cm.get(path)
      .then((model: Contents.IModel) => {
        console.log(`model content: \n${model.content}`);
        console.log(`model format: ${model.format}`);
        console.log(`model type: ${model.type}`);
      });
    let pngPath: string = 'test/data/asterisk.png';
    cm.get(pngPath)
      .then((model: Contents.IModel) => {
        console.log(`model name: ${model.name}`);
        console.log(`model format: ${model.format}`);
        console.log(`model type: ${model.type}`);
      }, (err:any) => {
        console.error(`Did NOT load ${pngPath}`);
        console.error(err);
      });
  },
};


const FormExtension: JupyterLabPlugin<void> = createRendermimePlugin(SimpleFormMimeExtension);
const extensions: JupyterLabPlugin<void>[] =  [ServicesExtension, CommExtension, FormExtension];
export default extensions;
