import { Kernel } from '@jupyterlab/services';
import {
  JSONValue,
  JSONObject
} from '@phosphor/coreutils';

import * as React from 'react';

export interface IProps {
  data?: JSONObject;
  metadata?: JSONObject;
  comm?: Kernel.IComm;
}

export interface IState {
  value: {
    inputText: string;
  },
  outputText: string;
}


/**
 * Simple widget with text input and button. When button is clicked,
 * the text is sent back to the kernel using the comm object passed in
 */
export default
class SimpleFormComponent extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    // console.log('props in SimpleFormComponent constructor:');
    // console.dir(props);
    super(props);

    // state.value is the data sent back to the kernel on the custom comm channel
    // state.outputText is only for local display (would typically be a separate component)
    this.state = {
      value: {
        inputText: '',
      },
      outputText: '(no output text)'
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }  // constructor()

  // See https://stackoverflow.com/a/42085792
  handleInput(event: React.FormEvent<HTMLInputElement>): void {
    this.setState({value: {inputText: event.currentTarget.value}});
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();

    this.setState({outputText: this.state.value.inputText});
    // console.log('In handleClick, props:');
    // console.dir(this.props);
    if (this.props.comm) {
      let data: JSONValue = {'value': this.state.value};
      this.props.comm.send(data, this.props.metadata);
    }
    else {
      console.log('No comm object');
    }
  }

  render() {
    return (
      <div>
        <h3>React Component (Simple Form)</h3>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Enter text"
            value={this.state.value.inputText}
            onChange={this.handleInput} />
          <input
            type="submit"
            value="Send"
            disabled={!this.state.value.inputText} />
        </form>
        <p>
          {this.state.outputText}
        </p>
      </div>
    );
  }
  
}  // SimpleFormComponent
